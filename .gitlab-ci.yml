variables:
  STABLE_VERSION: "0.14"
  STABLE_IMAGE_NAME: "$CI_REGISTRY_IMAGE/stable:latest"
  BUILD_IMAGE_NAME: "$CI_REGISTRY_IMAGE/branches/$CI_COMMIT_REF_SLUG-$TERRAFORM_VERSION:$CI_COMMIT_SHA"
  RELEASE_IMAGE_NAME: "$CI_REGISTRY_IMAGE/releases/$TERRAFORM_VERSION"
  TF_ADDRESS: "$CI_API_V4_URL/projects/$CI_PROJECT_ID/terraform/state/$CI_PIPELINE_IID-$STATE_NAME"
  TFPLANTOOL_VERSION: "v0.1.0"

.versions:
  parallel:
    matrix:
      - TERRAFORM_BASE: "hashicorp/terraform:0.14.5"
        TERRAFORM_VERSION: "0.14"
        STATE_NAME: terraform014
      - TERRAFORM_BASE: "hashicorp/terraform:0.13.6"
        TERRAFORM_VERSION: "0.13"
        STATE_NAME: terraform013
      - TERRAFORM_BASE: "hashicorp/terraform:0.12.30"
        TERRAFORM_VERSION: "0.12"
        STATE_NAME: terraform012

stages:
  - lint
  - build
  - test-init
  - test-validate
  - test-plan
  - test-apply
  - test-destroy
  - prepare-release
  - release

shell check:
  stage: lint
  image: koalaman/shellcheck-alpine:stable
  before_script:
    - shellcheck --version
  script:
    - shellcheck src/**/*.sh

.dind:
  services:
    - docker:19.03.5-dind
  image: docker:19.03.5
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"

build:
  extends:
    - .dind
    - .versions
  stage: build
  script:
    - docker image build --tag "$BUILD_IMAGE_NAME" --build-arg BASE=$TERRAFORM_BASE --build-arg TFPLANTOOL=$TFPLANTOOL_VERSION .
    - docker image push "$BUILD_IMAGE_NAME"

.test:
  image: "$BUILD_IMAGE_NAME"
  before_script:
    - gitlab-terraform version
    - jq --version
    - cd tests
  cache:
    key: "$TERRAFORM_VERSION-$CI_COMMIT_REF_SLUG"
    paths:
      - tests/.terraform/

test-init:
  extends:
    - .test
    - .versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - gitlab-terraform init

test-validate:
  extends:
    - .test
    - .versions
  stage: test-validate
  script:
    - gitlab-terraform validate

test-plan:
  extends:
    - .test
    - .versions
  stage: test-plan
  script:
    - gitlab-terraform plan
    - if [[ ! -f "plan.cache" ]]; then echo "expected to find a plan.cache file"; exit 1; fi
    - tfplantool -f plan.cache backend get -k username
    - gitlab-terraform plan-json
    - if [[ ! -f "plan.json" ]]; then echo "expected to find a plan.json file"; exit 1; fi
    - mv plan.cache $TERRAFORM_VERSION-plan.cache
  artifacts:
    paths:
      - "tests/*-plan.cache"

test-apply:
  extends:
    - .test
    - .versions
  stage: test-apply
  script:
    - mv $TERRAFORM_VERSION-plan.cache plan.cache
    - gitlab-terraform apply

test-destroy:
  extends:
    - .test
    - .versions
  stage: test-destroy
  script:
    - gitlab-terraform destroy

release:
  extends:
    - .dind
    - .versions
  stage: release
  script:
    - docker image pull "$BUILD_IMAGE_NAME"
    - docker image tag "$BUILD_IMAGE_NAME" "$RELEASE_IMAGE_NAME:latest"
    - docker image tag "$BUILD_IMAGE_NAME" "$CI_REGISTRY_IMAGE/releases/terraform:${TERRAFORM_BASE##*:}"
    - docker image tag "$BUILD_IMAGE_NAME" "$RELEASE_IMAGE_NAME:$CI_COMMIT_TAG"
    - docker image push "$RELEASE_IMAGE_NAME:latest"
    - docker image push "$CI_REGISTRY_IMAGE/releases/terraform:${TERRAFORM_BASE##*:}"
    - docker image push "$RELEASE_IMAGE_NAME:$CI_COMMIT_TAG"
    - if [ "$TERRAFORM_VERSION" = "$STABLE_VERSION" ]; then docker image tag "$BUILD_IMAGE_NAME" "$STABLE_IMAGE_NAME"; fi
    - if [ "$TERRAFORM_VERSION" = "$STABLE_VERSION" ]; then docker image push "$STABLE_IMAGE_NAME"; fi
  only:
    - tags

.semantic-release:
  image: node:12-buster-slim
  stage: prepare-release
  before_script:
    - apt-get update && apt-get install -y --no-install-recommends git-core ca-certificates
    - npm install -g semantic-release @semantic-release/gitlab
  script:
    - semantic-release $DRY_RUN_OPT -b $CI_COMMIT_REF_NAME

tag_release-dryrun:
  extends: .semantic-release
  variables:
    DRY_RUN_OPT: "-d"
  only:
    refs:
      - branches@gitlab-org/terraform-images
  except:
    refs:
      - master

tag_release:
  extends: .semantic-release
  only:
    refs:
      - master@gitlab-org/terraform-images
